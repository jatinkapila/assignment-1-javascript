/*
Great work Jatin.  Your application functions as required
and has the comments (html and JS) that I needed to see.
As well, everything is named correctly.  Well done
20/20
*/
function loadProvinces(){
  var body = document.querySelector('body');
  body.style.background = "tomato";

  var h2 = document.querySelector("h2");
  var isBlue = false;
  setInterval (function() {
    if (isBlue) {
      h2.style.color = "black";

    }else {
      h2.style.color = "blue";
    }
    isBlue = !isBlue;
  }, 1500);



    var provArray=["Alberta","British Columbia","Manitoba","New Brunswick"," Newfoundland and Labrador","Nova Scotia","Ontario","Prince Edward Island","Quebec","Saskatchewan"];
    var prov="<option value=''>-Select-</option>";
    for(p=0;p<provArray.length;p++){
        prov+="<option value='"+provArray[p]+"'>"+provArray[p]+"</option>";
    }
    document.getElementById('cboProv').innerHTML=prov;
}

function validateForm() {


    var name = document.getElementById('txtName').value;
    var email = document.getElementById('txtEmail').value;
    var emailFormat = /(\w+\.)*\w+@([a-zA-Z]+\.)+[a-zA-Z]{2,6}/;
    var province = document.getElementById('cboProv').value;

    if(name==""){
        alert('please enter your name');
        document.getElementById('txtName').focus();
        return false;
    }
    if(email==""){
        alert('please enter a valid email address');
        document.getElementById('txtEmail').focus();
        return false;
    }else{
                // there's some kind of value in the email
                // check the formatting
                if(!emailFormat.test(email)){  // '!' mean NOT
                    alert("please enter a valid email format");
                    return;
                }
            }



    if(province==""){
        alert('please select a province');
        document.getElementById('cboProv').focus();
        return false;
    }

    alert('Hey ! '+''+ name +','+ " This is your valid_email: " +  email + ' and you are from '+ '' + province);
}
